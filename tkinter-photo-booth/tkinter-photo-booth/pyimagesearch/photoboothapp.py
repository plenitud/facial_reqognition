# import the necessary packages
from __future__ import print_function
from PIL import Image
from PIL import ImageTk
import tkinter as tki
from tkinter import Text
from tkinter import Button
from tkinter import INSERT
from tkinter import END
import threading
import datetime
import imutils
import cv2
import os
import numpy as np
import pickle
import requests

url = 'https://faces-olayer.eu-gb.mybluemix.net/ask'  # API End-Point
data = '{"message": "Hello"}'

response = requests.post(url, data=data)  # REST API call GET/POST/DELETE

print(response)
print(response.json())

face_cascade = cv2.CascadeClassifier(
    'cascades/data/haarcascade_frontalface_alt2.xml')
eye_cascade = cv2.CascadeClassifier('cascades/data/haarcascade_eye.xml')
smile_cascade = cv2.CascadeClassifier('cascades/data/haarcascade_smile.xml')


recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("./recognizers/face-trainner.yml")

labels = {"person_name": 1}
with open("pickles/face-labels.pickle", 'rb') as f:
    og_labels = pickle.load(f)
    labels = {v: k for k, v in og_labels.items()}


class PhotoBoothApp:
    def __init__(self, vs, outputPath):
        # store the video stream object and output path, then initialize
        # the most recently read frame, thread for reading frames, and
        # the thread stop event
        self.vs = vs
        self.outputPath = outputPath
        self.frame = None
        self.thread = None
        self.stopEvent = None

        # initialize the root window and image panel
        self.root = tki.Tk()
        self.panel = None
        # You want the size of the app to be 500x500
        self.root.geometry("1500x500")
        # Don't allow resizing in the x or y direction
        self.root.resizable(0, 0)

        # create a button, that when pressed, will take the current
        # frame and save it to file
        # text = response.json()
        # btn = tki.Button(
        #    self.root, text=text["body"]["output"]["generic"][0]["text"], command=self.takeSnapshot)
        # btn.pack(side="bottom", fill="both", expand="yes", padx=10, pady=10)
        # text input
        textBox = Text(self.root, height=2, width=50)
        textBox.pack()
        buttonCommit = Button(self.root, height=1, width=10, text="Send",
                              command=lambda: retrieve_input())
        buttonCommit.pack()
        self.currentName = ""

        def retrieve_input():
            inputValue = textBox.get("1.0", "end-1c")
            print(inputValue)
            data = {"message": inputValue}
            print(data)
            if (self.currentName):
                w = tki.Label(self.root, text="Hi " +
                              self.currentName, bg="blue", fg="white")
                w.pack(fill=tki.X, side="top")

            w = tki.Label(self.root, text=inputValue, bg="red", fg="white")
            w.pack(fill=tki.X, side="top")
            response = requests.post(url, data=data)
            print(response.json())
            text = response.json()
            w = tki.Label(
                self.root, text=text["body"]["output"]["generic"][0]["text"], bg="blue", fg="white")
            w.pack(fill=tki.X, side="top")
        # create a button, that when pressed, will take the current
        # frame and save it to file
        # btn = tki.Button(self.root, text="Snapshot!",
        #                                 command=self.takeSnapshot)
        # btn.pack(side="bottom", fill="both", expand="yes", padx=10,
        #         pady=10)
        # start a thread that constantly pools the video sensor for
        # the most recently read frame
        self.stopEvent = threading.Event()
        self.thread = threading.Thread(target=self.videoLoop, args=())
        self.thread.start()

        # set a callback to handle when the window is closed
        self.root.wm_title("PyImageSearch PhotoBooth")
        self.root.wm_protocol("WM_DELETE_WINDOW", self.onClose)

    def videoLoop(self):
        # DISCLAIMER:
        # I'm not a GUI developer, nor do I even pretend to be. This
        # try/except statement is a pretty ugly hack to get around
        # a RunTime error that Tkinter throws due to threading
        try:
            # keep looping over frames until we are instructed to stop
            while not self.stopEvent.is_set():
                # grab the frame from the video stream and resize it to
                # have a maximum width of 300 pixels
                self.frame = self.vs.read()
                self.frame = imutils.resize(self.frame, width=750, height = 700)
                # OpenCV represents images in BGR order; however PIL
                # represents images in RGB order, so we need to swap
                # the channels, then convert to PIL and ImageTk format
                image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
                gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
                image = Image.fromarray(image)
                image = ImageTk.PhotoImage(image)
                # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                # gray = (np.float32(image), cv2.COLOR_RGB2GRAY)
                faces = face_cascade.detectMultiScale(
                    gray, scaleFactor=1.5, minNeighbors=5)
                for (x, y, w, h) in faces:
                    roi_gray = gray[y:y+h, x:x+w]
                    roi_color = self.frame[y:y+h, x:x+w]
                    # print(roi_color)
                    # recognize? deep learned model predict keras tensorflow pytorch scikit learn
                    id_, conf = recognizer.predict(roi_gray)
                    # id_, conf = recognizer.predict(cv2.imread('images/emilia-clarke/1.jpg', 0))
                    font = cv2.FONT_HERSHEY_SIMPLEX
                    name = labels[id_]

                    color = (255, 255, 255)
                    stroke = 2
                    print(conf)
                    if (conf >= 70):
                        self.currentName = name
                        cv2.putText(self.frame, "Hi " + name + ", welcome to Bcx.",
                                    (150, 150), font, 1, color, stroke, cv2.LINE_AA)
                    else:
                        cv2.putText(self.frame, "Unknown",
                                    (150, 150), font, 1, color, stroke, cv2.LINE_AA)
                    color = (0, 0, 204)  # BGR 0-255
                    stroke = 2
                    end_cord_x = x + w
                    end_cord_y = y + h
                    cv2.rectangle(self.frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
                    # OpenCV represents images in BGR order; however PIL
                    # represents images in RGB order, so we need to swap
                    # the channels, then convert to PIL and ImageTk format
                    image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
                    gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
                    image = Image.fromarray(image)
                    image = ImageTk.PhotoImage(image)

                    # subitems = smile_cascade.detectMultiScale(roi_gray)
                    # for (ex,ey,ew,eh) in subitems:
                    #	cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
                if self.panel is None:
                    self.panel = tki.Label(image=image)
                    self.panel.image = image
                    self.panel.pack(side="left", padx=10, pady=10)
                    # otherwise, simply update the panel
                else:
                    self.panel.configure(image=image)
                    self.panel.image = image
                # Not working , need a nice way to close program
                # cv2.imshow('frame', self.frame)
                if cv2.waitKey(20) & 0xFF == ord('q'):
                    self.stopEvent.set
                    break
        except RuntimeError as e:
            print("[INFO] caught a RuntimeError")

    def takeSnapshot(self):
        # grab the current timestamp and use it to construct the
        # output path
        ts = datetime.datetime.now()
        filename = "{}.jpg".format(ts.strftime("%Y-%m-%d_%H-%M-%S"))
        p = os.path.sep.join((self.outputPath, filename))

        # save the file
        cv2.imwrite(p, self.frame.copy())
        print("[INFO] saved {}".format(filename))

    def onClose(self):
        # set the stop event, cleanup the camera, and allow the rest of
        # the quit process to continue
        print("[INFO] closing...")
        self.stopEvent.set()
        self.vs.stop()
        self.root.quit()
